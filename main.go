package main

import (
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
 	"github.com/bigkevmcd/go-configparser"
	"strings"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"time"
	"context"
	"math"
)

var client *mongo.Client

type DNA struct {
	DNA []string `json:"dna,omitempty"`
}


type Statistics struct {
	Count_mutant_dna int32 `json:"count_mutant_dna,omitempty"`
	Count_human_dna int32 `json:"count_human_dna,omitempty"`
	Ratio float64 `json:"ratio,omitempty"`
}

var stats Statistics
var adn []DNA 


//The following const and function Dataparser sets the values to connect to Amazon DocumentDB
const (

	// Path to the AWS CA file
	caFilePath = "rds-combined-ca-bundle.pem"

	// Timeout operations after N seconds
	connectTimeout  = 5
	queryTimeout    = 30
	
	// Which instances to read from
	readPreference = "secondaryPreferred"

	connectionStringTemplate = "mongodb://%s:%s@%s/test?ssl=true&sslcertificateauthorityfile=%s&replicaSet=rs0&readpreference=%s"
)

func DataParser() (username string, password string ,clusterEndpoint string) {
    
        config, err := configparser.NewConfigParserFromFile("main.ini")
         if err != nil {
                log.Fatal(err)
         }
   
	username , _ = config.Get("DB","Username")
	password , _  = config.Get("DB","Password")
	clusterEndpoint , _  = config.Get("DB","ClusterEndpoint")
	
	return username , password, clusterEndpoint
}


func main() {


	router := mux.NewRouter()

	// endpoints
	router.HandleFunc("/", GetDefaultEndpoint).Methods("GET")
	router.HandleFunc("/stats", GetStatsEndpoint).Methods("GET")
	router.HandleFunc("/mutant/", PostDnaEndpoint).Methods("POST")

	//Turn on the Server
	log.Fatal(http.ListenAndServe(":3030", router))
}

//GET Testing Golang is UP
func GetDefaultEndpoint(w http.ResponseWriter, req *http.Request){
        json.NewEncoder(w).Encode("Golang is UP in Webserver 1")
}

//GET Method that Returns the Json when /stats is called
func GetStatsEndpoint(w http.ResponseWriter, req *http.Request){
	// Estadistica returns the json with the amount of mutant, humans and ratio.
	adnStats  := Estadistica(false,false)
	//fmt.Println(adnStats)
	json.NewEncoder(w).Encode(adnStats[0])
}

//POST Method that receive the JSON with dna
func PostDnaEndpoint(w http.ResponseWriter, req *http.Request){

	
	username , password , clusterEndPoint := DataParser()	
	//fmt.Println(username,password,clusterEndPoint)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	//It is commented the cluster Amazon DocumentDB ,not used in this example
	connectionURI := fmt.Sprintf(connectionStringTemplate, username, password,clusterEndPoint, caFilePath, readPreference)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionURI))

	//client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	collection := client.Database("dbMeli").Collection("dna")

	var exists DNA
	var adn DNA
	var valid bool
	var data []string
	
	decoder := json.NewDecoder(req.Body)
	
	err = decoder.Decode(&adn)

	//Check format of the Json received is ok, if not a message of error is sent
	if err != nil {
		//panic(err)
	        w.WriteHeader(403) 
		json.NewEncoder(w).Encode("POR FAVOR, ENVIE UN JSON VALIDO")
		return
	}
	data = adn.DNA
	
	valid, dna := CheckJson(data)
	//fmt.Println(valid)
	
	if valid {
			
	   //Check if dna exists in database, if it exists then discard it.
           filter := bson.M{"dna":dna}
 	   exist := collection.FindOne(ctx, filter).Decode(&exists)
           if exist == nil {
		//log.Fatal(err)
		w.WriteHeader(403) 
		json.NewEncoder(w).Encode("Ya existe el registro en nuestra base de datos.")
                return
           }
	
	 // If not exist in database continue searching if mutant or human
	   if IsMutant(data) {
	        _ , err := collection.InsertOne(ctx, adn)
                if err != nil {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode("adn MUTANTE error al intentar insertar a la base de datos")
		return
	     } else  {
		//I give the value of true in funcion Estadistica to increase in 1 the value of count_mutant_dna
		w.WriteHeader(200)
		Estadistica(true,false)
		json.NewEncoder(w).Encode("adn MUTANTE ingresado exitosamente a la base de datos")
		return
	     }
				
			
		// If Human returns 403-forbiden
	 } else { 
	        _ ,err := collection.InsertOne(ctx, adn)
		if err != nil {
		w.WriteHeader(403) 
 		json.NewEncoder(w).Encode("adn HUMANO error al intentar insertar a la base de datos")
		return
		}  else {
			//I give the value of true in funcion Estadistica to increase in 1 the value of count_human_dna
			w.WriteHeader(403)
			Estadistica(false,true)
			json.NewEncoder(w).Encode("adn HUMANO ingresado exitosamente a la base de datos")
			return
	 	  }
	   } 
	   
     } else {
	    // If Json sent by post is not valid e.g. the String is not NxN or it contains a letter differnt to (A,T,C,G)
	   //ADN is not valid
	   w.WriteHeader(403) 
           json.NewEncoder(w).Encode("Este adn 'NO ES VALIDO' solo debe contener las letras A,T,C,G y ser NxN")
	   return
     }	
}


// Function Estadistica recieves two variables type bool and returns the Json with following
// {"count_mutant_dna": 0, "count_human_dna": 0,"ratio":0.0}
// The Collection statistics has only one document that is updated when any new mutant or human is input

func Estadistica(isMutant bool, isHuman bool) ([]Statistics) {
	var adnStats []Statistics
	adnStats = []Statistics{{Count_mutant_dna: 0, Count_human_dna: 0,Ratio:0.0}}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	username , password , clusterEndPoint := DataParser()	
	//fmt.Println(username,password,clusterEndPoint)


        //It is commented the cluster Amazon DocumentDB ,not used in this example
        connectionURI := fmt.Sprintf(connectionStringTemplate, username, password,clusterEndPoint, caFilePath, readPreference)
        client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionURI))


	//client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	collection := client.Database("dbMeli").Collection("statistics")


	ctx, _ = context.WithTimeout(context.Background(), 30*time.Second)
	cur, err := collection.Find(ctx, bson.D{})
	if err != nil { 
		//log.Fatal(err)
		fmt.Println("Error con la conexion a la base de datos.")
	}
	defer cur.Close(ctx)

	//If collection statistics does not exist then I create it
	if !cur.Next(ctx){
		_, err := collection.InsertOne(ctx, bson.M{"count_mutant_dna": 0, "count_human_dna": 0,"ratio":0.0})
		if err != nil {
			//log.Fatal(err)
			fmt.Println("Error con la conexion a la base de datos.")
		}
		//id := res.InsertedID
		//fmt.Println(id)
	} 
	
	ctx, _ = context.WithTimeout(context.Background(), 30*time.Second)
	cur, err = collection.Find(ctx, bson.D{})
	if err != nil { 
		//log.Fatal(err)
		fmt.Println("Error con la conexion a la base de datos.")
	}
	defer cur.Close(ctx)

	for cur.Next(ctx){
		var result bson.M
		err := cur.Decode(&result)
		if err != nil { 
			//log.Fatal(err)
			fmt.Println("Error con la conexion a la base de datos.")
		}  
		
			//fmt.Println(result["count_human_dna"],result["count_mutant_dna"],result["ratio"])
			count_human_dna := result["count_human_dna"].(int32)
			count_mutant_dna := result["count_mutant_dna"].(int32)
			ratio := result["ratio"].(float64)
			var adnStats []Statistics
			adnStats = append(adnStats , Statistics{Count_mutant_dna: count_mutant_dna, Count_human_dna: count_human_dna,Ratio:ratio})
			// Returns only values and no modifications.
			if !isMutant && !isHuman || isMutant && isHuman {
				return adnStats
			}

			// Check is Mutant to add one to count_mutant_dna and update ratio
			if isMutant {
				count_mutant_dna = count_mutant_dna + 1
				//Check if count_human_dna is still in zero, then I make ratio = count_mutant_dna
				if count_human_dna == 0 {
					ratio = float64(count_mutant_dna)
				} else {
					ratio = math.Round(float64(count_mutant_dna)/float64(count_human_dna)*100)/100
				}
			
			//Check is Human to add one to count_human_dna and update ratio
			} else if isHuman {
				count_human_dna = count_human_dna + 1
				ratio = math.Round(float64(count_mutant_dna)/float64(count_human_dna)*100)/100
			}
			client.Database("dbMeli").Collection("statistics").Drop(nil)
			_, err = collection.InsertOne(ctx, bson.M{"count_mutant_dna": count_mutant_dna, "count_human_dna": count_human_dna,"ratio":ratio})
			if err != nil {
				//log.Fatal(err)
				fmt.Println("Error con la conexion a la base de datos.")
			} else {
				return adnStats
			}
		}
	
	if err := cur.Err(); err != nil {
		//log.Fatal(err)
		fmt.Println("Error con la conexion a la base de datos.")
	}
	return adnStats
} 

//Check if the String is NxN and has only (A,T,C,G) ,if I am recieving something wrong discard this Json
//Return true if it is a valid string or false if it is not a valid String
func CheckJson(m []string) (bool ,[]string) {

	// Convert all strings inside array in uppercases
	for i:=0 ; i<len(m) ; i++ {
		m[i] = strings.ToUpper(m[i])
	}
	
	//Check each string inside the arrray 
	//Check the length(array) = lenght (string). If not the data is wrong, returns false
	//Check the string only constains the letters (A,T,C,G). If not the data is wrong, returns false
	for i:=0 ; i<len(m) ; i++ {
		if len(m) != len(m[i]) { 
			//fmt.Println(m[i], " ", i) 
			return false , m }
		if strings.Trim(m[i] , "ATCG") != "" { 
			//fmt.Println(strings.Trim(m[i] , "ATCG") , "  ", i) 
			return false , m}
		}
	return true , m
}

// IsMutant returns true if find in dna two consequences
func IsMutant(m []string) bool {
	
	// Takes the lenght of the array
	l:= len(m)

	// Set Variables
	// count is the times of consequences found
	count := 0

	//wordHorizontal is built of 4 letters in horizontal position
	wordHorizontal := ""

	//wordVertival is built of 5 letters in Vertical position
	wordVertical := ""

	//wordObliqueRight is built of 5 letters in Oblique towards Right position 
	wordObliqueRight := ""

	//wordObliqueLeft is built of 5 letters in Oblique towards Left position.
	wordObliqueLeft := ""

	//This "for" build all possible words founded in different positions.
	// After built a word, then compare if exists consequence and increase count in 1 unit
	//If count reaches 2 consequences the func IsMutant returns true and stop searching if not continue searching
	for i:=0 ; i<l ; i++ {
		for j:=0 ; j<l-3 ; j++ {

			//Catch 4 letters horizontal&consecutive, compare if they are all equals and if true the variable count is increased in 1 unit 
			wordHorizontal = (m[i][j:j+4])
			count = count + CompareSequence(wordHorizontal)
			//if CompareSequence(wordHorizontal) == 1 {fmt.Println(wordHorizontal)}
			//If we find two consequences we finish searching and return true, if not we continue searching
			if count > 1 {return true}

			//Catch 4 letters vertical&consecutive, compare if they are all equals and if true the variable count is increased in 1 unit 
			wordVertical = string(m[j][i]) + string(m[j+1][i]) + string(m[j+2][i]) + string(m[j+3][i])
			count = count + CompareSequence(wordVertical)
 			//if CompareSequence(wordHorizontal) == 1 {fmt.Println(wordHorizontal)}
			
			//If we find two consequences we finish searching and return true, if not we continue searching
			if count >1 {return true}

			if i<3 {
				//Catch 4 letters Oblique towards the Right&consecutive, compare if they are all equals and if true we increase the count in 1 unit
				wordObliqueRight = string(m[i][j]) + string(m[i+1][j+1]) + string(m[i+2][j+2]) + string(m[i+3][j+3])
				count = count + CompareSequence(wordObliqueRight)
				//if CompareSequence(wordObliqueRight) == 1 {fmt.Println(wordObliqueRight)}			
			
				//If we find two consequences we finish searching and return true, if not we continue searching
				if count >1 {return true}

				//Catch 4 letters Oblique towards the Left&consecutive, compare if they are all equals and if true we increase the count in 1 unit
				wordObliqueLeft = string(m[i][5-j]) + string(m[i+1][4-j]) + string(m[i+2][3-j]) + string(m[i+3][2-j])
				count = count + CompareSequence(wordObliqueLeft)
				//if CompareSequence(wordObliqueLeft) == 1 {fmt.Println(wordObliqueLeft)}			
								
				//If we find two consequences we finish searching and return true, if not we continue searching
				if count >1 {return true}
			}
		}
	}
	//If there not exist two consequences then return false
	return false
}

//CompareSequence returns 1 if the word searched is "AAAA" or "CCCC" or "GGGG" or "TTTT", if not returns 0
func CompareSequence(sequence string) int{
	// If a consequence if found, we return 1 , if not we do continue comparing
	if strings.Contains(sequence, "AAAA"){ 
		return 1
		} else if strings.Contains(sequence, "CCCC"){ 
			return 1
		} else if strings.Contains(sequence, "GGGG"){ 
			return 1
		} else if strings.Contains(sequence, "TTTT"){ 
			return 1
		}
	return 0
	}


